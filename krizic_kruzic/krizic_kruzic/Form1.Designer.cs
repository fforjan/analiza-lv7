﻿namespace krizic_kruzic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.A1 = new System.Windows.Forms.PictureBox();
            this.A2 = new System.Windows.Forms.PictureBox();
            this.A3 = new System.Windows.Forms.PictureBox();
            this.B3 = new System.Windows.Forms.PictureBox();
            this.B2 = new System.Windows.Forms.PictureBox();
            this.B1 = new System.Windows.Forms.PictureBox();
            this.C3 = new System.Windows.Forms.PictureBox();
            this.C2 = new System.Windows.Forms.PictureBox();
            this.C1 = new System.Windows.Forms.PictureBox();
            this.lb_current = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_x = new System.Windows.Forms.TextBox();
            this.tb_o = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).BeginInit();
            this.SuspendLayout();
            // 
            // A1
            // 
            this.A1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.A1.Location = new System.Drawing.Point(12, 111);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(75, 75);
            this.A1.TabIndex = 0;
            this.A1.TabStop = false;
            this.A1.Tag = "";
            this.A1.Click += new System.EventHandler(this.PB_click);
            // 
            // A2
            // 
            this.A2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.A2.Location = new System.Drawing.Point(93, 111);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(75, 75);
            this.A2.TabIndex = 1;
            this.A2.TabStop = false;
            this.A2.Tag = "";
            this.A2.Click += new System.EventHandler(this.PB_click);
            // 
            // A3
            // 
            this.A3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.A3.Location = new System.Drawing.Point(174, 111);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(75, 75);
            this.A3.TabIndex = 2;
            this.A3.TabStop = false;
            this.A3.Tag = "";
            this.A3.Click += new System.EventHandler(this.PB_click);
            // 
            // B3
            // 
            this.B3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.B3.Location = new System.Drawing.Point(174, 192);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(75, 75);
            this.B3.TabIndex = 5;
            this.B3.TabStop = false;
            this.B3.Tag = "";
            this.B3.Click += new System.EventHandler(this.PB_click);
            // 
            // B2
            // 
            this.B2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.B2.Location = new System.Drawing.Point(93, 192);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(75, 75);
            this.B2.TabIndex = 4;
            this.B2.TabStop = false;
            this.B2.Tag = "";
            this.B2.Click += new System.EventHandler(this.PB_click);
            // 
            // B1
            // 
            this.B1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.B1.Location = new System.Drawing.Point(12, 192);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(75, 75);
            this.B1.TabIndex = 3;
            this.B1.TabStop = false;
            this.B1.Tag = "";
            this.B1.Click += new System.EventHandler(this.PB_click);
            // 
            // C3
            // 
            this.C3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.C3.Location = new System.Drawing.Point(174, 273);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(75, 75);
            this.C3.TabIndex = 8;
            this.C3.TabStop = false;
            this.C3.Tag = "";
            this.C3.Click += new System.EventHandler(this.PB_click);
            // 
            // C2
            // 
            this.C2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.C2.Location = new System.Drawing.Point(93, 273);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(75, 75);
            this.C2.TabIndex = 7;
            this.C2.TabStop = false;
            this.C2.Tag = "";
            this.C2.Click += new System.EventHandler(this.PB_click);
            // 
            // C1
            // 
            this.C1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.C1.Location = new System.Drawing.Point(12, 273);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(75, 75);
            this.C1.TabIndex = 6;
            this.C1.TabStop = false;
            this.C1.Tag = "";
            this.C1.Click += new System.EventHandler(this.PB_click);
            // 
            // lb_current
            // 
            this.lb_current.AutoSize = true;
            this.lb_current.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_current.Location = new System.Drawing.Point(144, 80);
            this.lb_current.Name = "lb_current";
            this.lb_current.Size = new System.Drawing.Size(70, 26);
            this.lb_current.TabIndex = 9;
            this.lb_current.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 24);
            this.label2.TabIndex = 10;
            this.label2.Text = "Trenutno na potezu:";
            // 
            // tb_x
            // 
            this.tb_x.Location = new System.Drawing.Point(12, 31);
            this.tb_x.Name = "tb_x";
            this.tb_x.Size = new System.Drawing.Size(100, 22);
            this.tb_x.TabIndex = 11;
            this.tb_x.Text = "Igrač 1";
            // 
            // tb_o
            // 
            this.tb_o.Location = new System.Drawing.Point(149, 31);
            this.tb_o.Name = "tb_o";
            this.tb_o.Size = new System.Drawing.Size(100, 22);
            this.tb_o.TabIndex = 12;
            this.tb_o.Text = "Igrač 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(181, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "O";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(265, 359);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_o);
            this.Controls.Add(this.tb_x);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lb_current);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Križić-Kružić";
            ((System.ComponentModel.ISupportInitialize)(this.A1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox A1;
        private System.Windows.Forms.PictureBox A2;
        private System.Windows.Forms.PictureBox A3;
        private System.Windows.Forms.PictureBox B3;
        private System.Windows.Forms.PictureBox B2;
        private System.Windows.Forms.PictureBox B1;
        private System.Windows.Forms.PictureBox C3;
        private System.Windows.Forms.PictureBox C2;
        private System.Windows.Forms.PictureBox C1;
        private System.Windows.Forms.Label lb_current;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_x;
        private System.Windows.Forms.TextBox tb_o;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

