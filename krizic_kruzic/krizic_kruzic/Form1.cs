﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krizic_kruzic
{
    public partial class Form1 : Form
    {
        //Napravite igru križić-kružić(iks-oks) korištenjem znanja stečenih na ovoj
        //laboratorijskoj vježbi.Omogućiti pokretanje igre, unos imena dvaju igrača, ispis
        //koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na
        //odgovarajućim mjestima te ispis dijaloga s porukom o pobjedi, odnosno
        //neriješenom rezultatu kao i praćenje ukupnog rezultata.
        bool turn = true;
        int count = 0;
        public Form1()
        {
            InitializeComponent();
            lb_current.Text = tb_x.Text;
        }

        private void PB_click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            SolidBrush s = new SolidBrush(Color.Black);
            Graphics g = pb.CreateGraphics();
            FontFamily ff = new FontFamily("Arial");
            System.Drawing.Font font = new System.Drawing.Font(ff, 25);

            if (String.IsNullOrEmpty((string)pb.Tag))
            {
                if (turn)
                {
                    lb_current.Text = tb_o.Text;
                    g.DrawString("X", font, s, new PointF(12, 12));
                    pb.Tag = "x";
                }
                else
                {
                    lb_current.Text = tb_x.Text;
                    g.DrawString("O", font, s, new PointF(12, 12));
                    pb.Tag = "o";
                }
                count++;
                check_winner();
                turn = !turn;
                
                
                
            }

        }
        private void check_winner()
        {
            bool someone_won = false;


            if (((string)A1.Tag == (string)A2.Tag) && ((string)A2.Tag == (string)A3.Tag) && !(String.IsNullOrEmpty((string)A1.Tag)))
                someone_won = true;
            else if (((string)B1.Tag == (string)B2.Tag) && ((string)B2.Tag == (string)B3.Tag) && !(String.IsNullOrEmpty((string)B1.Tag)))
                someone_won = true;
            else if (((string)C1.Tag == (string)C2.Tag) && ((string)C2.Tag == (string)C3.Tag) && !(String.IsNullOrEmpty((string)C1.Tag)))
                someone_won = true;

            else if (((string)A1.Tag == (string)B1.Tag) && ((string)B1.Tag == (string)C1.Tag) && !(String.IsNullOrEmpty((string)A1.Tag)))
                someone_won = true;
            else if (((string)A2.Tag == (string)B2.Tag) && ((string)B2.Tag == (string)C2.Tag) && !(String.IsNullOrEmpty((string)A2.Tag)))
                someone_won = true;
            else if (((string)A3.Tag == (string)B3.Tag) && ((string)B3.Tag == (string)C3.Tag) && !(String.IsNullOrEmpty((string)A3.Tag)))
                someone_won = true;

            else if (((string)A3.Tag == (string)B2.Tag) && ((string)B2.Tag == (string)C1.Tag) && !(String.IsNullOrEmpty((string)A3.Tag)))
                someone_won = true;
            else if (((string)A1.Tag == (string)B2.Tag) && ((string)B2.Tag == (string)C3.Tag) && !(String.IsNullOrEmpty((string)A1.Tag)))
                someone_won = true;


            if (someone_won)
            {
                string winner = "";
                if (turn)
                    winner = tb_x.Text;
                else
                    winner = tb_o.Text;
                MessageBox.Show(winner + " Wins!", "FINNISH");
                new_game();
            }
            else
            {   
                if (count == 9)
                {
                    MessageBox.Show("TIE!", "FINNISH!");
                    new_game();
                }
            }
                
        }
        private void new_game()
        {
            turn = true;
            count = 0;

            A1.Image = null;
            A1.Tag = "";

            A2.Image = null;
            A2.Tag = "";

            A3.Image = null;
            A3.Tag = "";

            B1.Image = null;
            B1.Tag = "";

            B2.Image = null;
            B2.Tag = "";

            B3.Image = null;
            B3.Tag = "";

            C1.Image = null;
            C1.Tag = "";

            C2.Image = null;
            C2.Tag = "";

            C3.Image = null;
            C3.Tag = "";
        }


    }
}
